# Proton alchemist

Proton alchemist is a package that facilitates alchemical calculation
of pKa for common amino acids: ASP, GLU, HIS and LYS.

## Requirements

Currently, there are no dependencies beyond the standard Python libraries.
However, since charges and atomtypes are read from Gromacs .rtp files,
paths to valid ffbonded.itp and aminoacids.rtp (or merged.rtp) files
should be specified (see below).

## Standard usage

A simple run includes the following options:

```
python ./skrypt.py -p protein_topology.itp -n residue_number -r residue_name -b path_to_gromacs/myforcefield.ff/ffbonded.itp
-d path_to_gromacs/myforcefield.ff/aminoacids.rtp
```

Note that you should start with a protonated topology (neutral ASP/GLU,
cationic HIS/LYS) -- the script itself does not introduce any additional
atoms, which is why the corresponding .pdb/.gro file should be valid
with no extra modifications.

The additional `--switch` option acts as a flag, swapping states A and B;
this can be helpful for runs where the reference molecule is a part of the
same system, e.g. as one ASP inside a protein is being deprotonated, another
free ASP (restrained to bulk solvent) is being protonated, thus preserving
the total charge of the system. This approach solves two problems at once:
one is that of keeping the charge neutral, and another of running reference
simulations in pure solvent to find the free energy change with respect
to aqueous solvent conditions.

When in doubt, consult tutorials and online resources, e.g. the excellent
Alchemistry Wiki:
http://alchemistry.org/wiki/Charged_binding_calculations
